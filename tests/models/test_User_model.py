from django.test import TestCase
from tasker.managers import UserManager


class User_Model_Test(TestCase):
    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        UserManager.create(first_name='Name',
                           last_name='Surname',
                           email='user@mail.org',
                           username='Login',
                           password='Password123!')

    def test_creating_user(self):
        self.assertIsNotNone(UserManager.safe_get(username='Login'))

    def test_deleting_user(self):
        user = UserManager.safe_get(username='Login')

        self.assertTrue(UserManager.safe_delete(user))


