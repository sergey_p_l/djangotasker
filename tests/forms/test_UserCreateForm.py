from django.test import TestCase
from tasker.forms import UserCreateForm


class UserCreateForm_Form_Test(TestCase):
    def setUp(self):
        pass

    def test_UserCreateForm_valid(self):
        form = UserCreateForm(data={'first_name': "Name",
                                    'last_name': "Surname",
                                    'email': "user@mail.org",
                                    'username': "NewLogin",
                                    'password1': "Password123!",
                                    'password2': "Password123!"})
        self.assertTrue(form.is_valid())

    # Form UserCreateForm
    def test_UserCreateForm_not_valid(self):
        form = UserCreateForm(data={'first_name': "Name",
                                    'last_name': "Surname",
                                    'email': "user@mail.org",
                                    'username': "NewLogin",
                                    'password1': "Password123!",
                                    'password2': "Password123!-"})
        self.assertFalse(form.is_valid())
