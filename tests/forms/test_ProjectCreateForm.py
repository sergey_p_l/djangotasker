from django.test import TestCase
from tasker.forms import ProjectCreateForm
from tasker.managers import UserManager


class ProjectCreateForm_Test(TestCase):
    def setUp(self):
        UserManager.create(first_name='Name',
                           last_name='Surname',
                           email='user@mail.org',
                           username='Login',
                           password='Password123!',
                           is_manager=True)

    def test_ProjectCreateForm_valid(self):
        form = ProjectCreateForm(data={'title': "Project1",
                                       'description': "Description of Project",
                                       'manager': 1})
        self.assertTrue(form.is_valid())

    def test_ProjectCreateForm_not_valid(self):
        form = ProjectCreateForm(data={'title': "",
                                       'description': "Description of Project",
                                       'manager': 1})
        self.assertFalse(form.is_valid())
