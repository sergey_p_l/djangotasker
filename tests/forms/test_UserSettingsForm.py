from django.test import TestCase
from tasker.forms import UserSettingForm
from tasker.managers import UserManager


class UserSettingForm_Test(TestCase):
    def setUp(self):
        UserManager.create(first_name='Name',
                           last_name='Surname',
                           email='user@mail.org',
                           username='Login',
                           password='Password123!')

    def test_UserSetting_valid(self):
        form = UserSettingForm(data={'first_name': "Name",
                                     'last_name': 'Surname',
                                     'email': "user@mail.org",
                                     'username': "NewLogin"})
        self.assertTrue(form.is_valid())

    def test_UserSetting_not_valid(self):
        form = UserSettingForm(data={'first_name': "Name",
                                     'last_name': "Surname",
                                     'email': "user mail.org",
                                     'username': "Login"})
        self.assertFalse(form.is_valid())
