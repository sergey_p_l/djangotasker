from django.test import TestCase
from tasker.forms import TaskCreateForm
from tasker.managers import UserManager
from tasker.managers import ProjectManager


class TaskCreateForm_Test(TestCase):
    def setUp(self):
        self.manager = UserManager.create(first_name='Name',
                                          last_name='Surname',
                                          email='user@mail.org',
                                          username='Login',
                                          password='Password123!',
                                          is_manager=True)
        self.developer = UserManager.create(first_name='NewName',
                                            last_name='NewSurname',
                                            email='Newuser@mail.org',
                                            username='NewLogin',
                                            password='Password123!',
                                            is_manager=False)
        self.project = ProjectManager.create(title="Project1",
                                             description="Description of project",
                                             slug='project1',
                                             manager=self.manager)

    def test_TaskCreateForm_valid(self):
        form = TaskCreateForm(data={'title': "Task1",
                                    'description': "Description of Task",
                                    'project': self.project.id,
                                    'developer': self.developer.id})
        self.assertTrue(form.is_valid())

    def test_TaskCreateForm_not_valid(self):
        form = TaskCreateForm(data={'title': "",
                                    'description': "Description of Task",
                                    'project': self.project.id,
                                    'developer': self.developer.id})
        self.assertFalse(form.is_valid())
