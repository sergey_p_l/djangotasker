from django.test import TestCase
from tasker.forms import SubTaskCreateForm
from tasker.managers import UserManager, TaskManager, ProjectManager


class SubTaskCreateForm_Test(TestCase):
    def setUp(self):
        self.manager = UserManager.create(first_name='Name',
                                          last_name='Surname',
                                          email='user@mail.org',
                                          username='Login',
                                          is_manager=True)
        self.developer = UserManager.create(first_name='NewName',
                                            last_name='NewSurname',
                                            email='Newuser@mail.org',
                                            username='NewLogin',
                                            is_manager=False)
        self.proj = ProjectManager.create(title="Project1",
                                          description="Description of Project",
                                          manager=self.manager)
        self.task = TaskManager.create(title="Task1",
                                       description="Description of Task",
                                       project=self.proj,
                                       developer=self.developer)

    def test_SubTaskCreateForm_valid(self):
        form = SubTaskCreateForm(data={'title': "Project1",
                                       'description': "Description of Project",
                                       'host_task': self.task.id,
                                       'developer': self.developer.id})
        self.assertTrue(form.is_valid())

    def test_SubTaskCreateForm_not_valid(self):
        form = SubTaskCreateForm(data={'title': "",
                                       'description': "Description of Project",
                                       'host_task': self.task.id,
                                       'developer': self.developer.id})
        self.assertFalse(form.is_valid())
