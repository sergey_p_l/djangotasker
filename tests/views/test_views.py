from django.test import TestCase

from tasker.managers import UserManager, ProjectManager, TaskManager, SubtaskManager


class Views_by_anonymous_user_test(TestCase):
    def setUp(self):
        self.client.logout()
        self.manager = UserManager.create(username="ManLogin",
                                          first_name="ManName",
                                          last_name="ManSurname",
                                          email="ManUser@mail.org",
                                          is_manager=True)
        self.manager.set_password('Password2019!')
        self.manager.save()

        self.developer = UserManager.create(username="DevLogin",
                                            first_name="DevName",
                                            last_name="DevSurname",
                                            email="devUser@mail.org",
                                            is_manager=False)
        self.developer.set_password('Password2019!')
        self.developer.save()

        self.project = ProjectManager.create(title="Project1",
                                             slug="project1",
                                             description="Description of Project",
                                             manager=self.manager)
        self.task = TaskManager.create(title="Task1",
                                       slug="task1",
                                       description="Description of Task",
                                       project=self.project,
                                       developer=self.developer)

        self.subtask = SubtaskManager.create(title="Subtask1",
                                             slug="subtask1",
                                             description="Description of Subtask",
                                             host_task=self.task,
                                             developer=self.developer)

    # BASE
    def test_home_view(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_about_view(self):
        response = self.client.get("/about/")
        self.assertEqual(response.status_code, 200)

    def test_search_view(self):
        response = self.client.get("/search/")
        self.assertEqual(response.status_code, 200)

    # USERS
    def test_create_user_view(self):
        response = self.client.get("/create-user/")
        self.assertEqual(response.status_code, 200)

    def test_profile_view(self):
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 302)

    def test_profile_user_view(self):
        response = self.client.get(f"/profile/{self.developer.username}/")
        self.assertEqual(response.status_code, 302)

    def test_profile_settings_view(self):
        response = self.client.get("/profile-settings/")
        self.assertEqual(response.status_code, 302)

    # PROJECTS
    def test_projects_view(self):
        response = self.client.get("/projects/")
        self.assertEqual(response.status_code, 200)

    def test_project_show_view(self):
        response = self.client.get(f"/project/{self.project.slug}/")
        self.assertEqual(response.status_code, 200)

    # TASK
    def test_task_show_view(self):
        response = self.client.get(f"/task/{self.task.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_task_create_view(self):
        response = self.client.get("/create-task/")
        self.assertEqual(response.status_code, 302)

    def test_task_edit_view(self):
        response = self.client.get(f"/edit-task/{self.task.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_task_complete_view(self):
        response = self.client.get(f"/mark-completed-task/{self.task.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_task_cancel_view(self):
        response = self.client.get(f"/mark-canceled-task/{self.task.slug}/")
        self.assertEqual(response.status_code, 302)

    # SUBTASKS
    def test_subtask_show_view(self):
        response = self.client.get(f"/subtask/{self.subtask.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_subtask_create_view(self):
        response = self.client.get(f"/create-subtask/{self.task.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_subtask_edit_view(self):
        response = self.client.get(f"/edit-subtask/{self.subtask.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_subtask_complete_view(self):
        response = self.client.get(f"/mark-complete-subtask/{self.subtask.slug}/")
        self.assertEqual(response.status_code, 302)

    def test_subtask_cancel_view(self):
        response = self.client.get(f"/mark-canceled-subtask/{self.subtask.slug}/")
        self.assertEqual(response.status_code, 302)

    # AUTH
    def test_login_view(self):
        response = self.client.get("/login/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")

    def test_logout_view(self):
        response = self.client.get("/logout/")
        self.assertEqual(response.status_code, 302)

    # DELETING
    def test_delete_task_view(self):
        response = self.client.get(f"/delete-task/{self.task}/")
        self.assertEqual(response.status_code, 302)

    def test_delete_subtask_view(self):
        response = self.client.get(f"/delete-subtask/{self.subtask}/")
        self.assertEqual(response.status_code, 302)


class Views_by_authenticate_user_manager_test(TestCase):
    def setUp(self):
        user = UserManager.create(username="Login",
                                  first_name="Name",
                                  last_name="Surname",
                                  email="user@mail.org",
                                  is_manager=True)
        user.set_password('Password2019!')
        user.save()

        self.client.logout()
        user_login = self.client.login(username="Login", password="Password2019!")
        self.assertTrue(user_login)

    def test_create_user_view(self):
        response = self.client.get("/create-user/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "models_actions/subtask_create.html")

    def test_create_task_view(self):
        response = self.client.get("/create-task/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "models_actions/subtask_create.html")
    #
    # def test_create_project_view(self):
    #     response = self.client.get("/create-project/")
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, "models_actions/subtask_create.html")

    # # Invalid Data
    # def test_add_user_invalidform_view(self):
    #     response = self.client.post("include url to post the data given",
    #                                 {'email': "admin@mp.com", 'password': "", 'first_name': "mp", 'phone': 12345678})
    #     self.assertTrue('"error": true' in response.content)
    #
    # # Valid Data
    # def test_add_admin_form_view(self):
    #     user_count = UserManager.count()
    #     response = self.client.post("include url to post the data given",
    #                                 {'email': "user@mp.com", 'password': "user", 'first_name': "user"})
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(UserManager.count(), user_count + 1)
    #     self.assertTrue('"error": false' in response.content)
