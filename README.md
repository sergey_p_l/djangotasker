# Welcome 
This project was created for study the Python Django framework v2.2

### 
### Download
Execute command:

`git clone https://sergey_p_l@bitbucket.org/sergey_p_l/djangotasker.git`

### Run
Go to **djangotasker** repository and run server by execution of the script **runserver.sh**:

`sh runserver.py`

Server is run and ready for work. To see the work of the site, open any browser and go to **localhost:8000**.
To configure the site go **/DjangoTasker** and update settings in **settings.py**.

## Tests 
To run tests go to tests folder and run `python manage.py test`.

## Authors
* Sergey Pivovar - **BSUIR**
