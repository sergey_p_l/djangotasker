from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views import View
from django.views.generic import UpdateView, DeleteView, CreateView, DetailView

from tasker.accesses import ManagerRequiredMixin, DeveloperRequiredMixin
from tasker.decorators import with_manager_access, with_developer_access
from tasker.managers import UserManager, ProjectManager, TaskManager, SubtaskManager
from tasker.models import Task, Subtask, Project

from .encryptors import account_activation_token
from .forms import UserCreateForm, UserSettingForm


def main_view(request):
    return render(request, 'main/main_page.html')


def access_error_view(request):
    return render(request, 'errors/error_access.html')


def projects_view(request):
    projects = ProjectManager.get_queryset()
    return render(request, 'projects/projects_page.html', context={'projects': projects})


class DetailsProjectView(LoginRequiredMixin, ManagerRequiredMixin, DetailView):
    model = Project
    template_name = 'models_actions/project/project_details.html'


class DetailsTaskView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = 'models_actions/task/task_details.html'


class DetailsSubtaskView(LoginRequiredMixin, ManagerRequiredMixin, DetailView):
    model = Subtask
    template_name = 'models_actions/subtask/subtask_details.html'


def about_view(request):
    return render(request, 'about/about_page.html')


@login_required
def profile_view(request, slug=None):
    if request.method == 'GET':
        if slug:
            user = UserManager.safe_get(slug__iexact=slug.lower())
            projects = user.get_all_projects()
            return render(request, 'profile/profile_page.html',
                          context={'user': user, 'projects': projects, 'request_user': request.user})
        else:
            projects = request.user.get_all_projects()
            return render(request, 'profile/profile_page.html',
                          context={'user': request.user, 'projects': projects, 'request_user': request.user})


@login_required
def profile_settings_view(request):
    form_change_password = PasswordChangeForm(user=request.user, data=request.POST or None)
    if request.method == "GET":
        form = UserSettingForm(instance=request.user)
        return render(request, 'profile/profile_settings.html', context={'user': request.user, 'form': form,
                                                                         'password_form': form_change_password})
    elif request.method == "POST" and "category" in request.POST:
        if request.POST['category'] == 'info_change':
            form = UserSettingForm(request.POST, instance=request.user)
            if form.is_valid():
                form.save()
                return redirect('profile_url')
            return render(request, 'profile/profile_settings.html', context={'user': request.user, 'form': form,
                                                                             'password_form': form_change_password})
        elif request.POST['category'] == 'pass_change':
            if form_change_password.is_valid():
                form_change_password.save()
                return redirect('profile_url')
            form = UserSettingForm(request.POST, instance=request.user)
            return render(request, 'profile/profile_settings.html', context={'user': request.user, 'form': form,
                                                                             'password_form': form_change_password})


class CreateTaskView(LoginRequiredMixin, ManagerRequiredMixin, CreateView):
    extra_context = {'model_type': 'Task'}
    model = Task
    template_name = 'models_actions/task/task_create.html'
    success_url = reverse_lazy('profile_url')
    fields = ['title', 'description', 'project', 'developer', 'date_completion']


class CreateSubtaskView(LoginRequiredMixin, DeveloperRequiredMixin, CreateView):
    extra_context = {'model_type': 'Subtask'}
    model = Subtask
    template_name = 'models_actions/subtask/subtask_create.html'
    success_url = reverse_lazy('profile_url')
    fields = ['title', 'description', 'host_task', 'developer']


def create_user_view(request):
    form = UserCreateForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        user = form.save()
        user.is_active = False
        user.save()
        return redirect('confirm_mail_url', slug=user.username)
    return render(request, 'models_actions/user/user_create.html', context={'form': form, 'model_type': 'User'})


class EditTaskView(LoginRequiredMixin, ManagerRequiredMixin, UpdateView):
    model = Task
    fields = ['title', 'description', 'project', 'developer', 'date_completion']
    template_name = 'models_actions/task/task_update.html'
    extra_context = {'model_type': 'Task'}
    success_url = reverse_lazy('profile_url')


class EditSubtaskView(LoginRequiredMixin, DeveloperRequiredMixin, UpdateView):
    model = Subtask
    fields = ['title', 'description', 'host_task', 'developer']
    template_name = 'models_actions/subtask/subtask_update.html'
    extra_context = {'model_type': 'Subtask'}
    success_url = reverse_lazy('profile_url')


class DeleteTaskView(LoginRequiredMixin, ManagerRequiredMixin, DeleteView):
    model = Task
    template_name = 'models_actions/task/task_delete.html'
    success_url = reverse_lazy('profile_url')


class DeleteSubtaskView(LoginRequiredMixin, DeveloperRequiredMixin, DeleteView):
    model = Task
    template_name = 'models_actions/subtask/subtask_delete.html'
    success_url = reverse_lazy('profile_url')


@login_required
def mark_task_is_completed_view(request, slug):
    if TaskManager.mark_completed(slug):
        return redirect('profile_url')
    else:
        return Http404()


@login_required
@with_manager_access
def mark_task_is_canceled_view(request, slug):
    if TaskManager.mark_canceled(slug):
        return redirect('profile_url')
    else:
        return Http404()


@login_required
@with_developer_access
def mark_subtask_is_completed_view(request, slug):
    if SubtaskManager.mark_completed(slug):
        return redirect('profile_url')
    else:
        return Http404()


@login_required
@with_developer_access
def mark_subtask_is_canceled_view(request, slug):
    if SubtaskManager.mark_canceled(slug):
        return redirect('profile_url')
    else:
        return Http404()


def activate_mail_view(request, index, token):
    try:
        uid = force_text(urlsafe_base64_decode(index))
        user = UserManager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, UserManager.model.DoesNotExist):
        user = None
    if user and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return redirect('user_login_url')
    else:
        return HttpResponse('Activation link is invalid!')


def send_confirm_mail_view(request, slug):
    user = UserManager.get_or_404(slug__iexact=slug.lower())
    if not user.is_active:
        mail_subject = 'Activate your blog profile.'
        message = render_to_string('email/confirm_email.html', {
            'user': user,
            'domain': get_current_site(request).domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        EmailMessage(mail_subject, message, to=[user.email]).send()
        return render(request, 'email/confirm_profile_by_email.html', context={'email': user.email})
    return redirect('main_url')


class SearchView(View):
    @staticmethod
    def get(request):
        query_sets = {}
        query = request.GET['search']
        if query:
            model_list = {'Users': UserManager,
                          'Projects': ProjectManager,
                          'Tasks': TaskManager,
                          'SubTasks': SubtaskManager,
                          }
            for title, manager in model_list.items():
                query_sets.update({title: manager.search(query)})

        return render(request, 'search/search_page.html', context={'results': query_sets, 'query': query})


################################################
#
#   EXTENDS
#
################################################

def delete_or_err404(model_manager, slug):
    if model_manager.delete_with_slug(slug):
        return redirect('profile_url')
    else:
        return Http404()

# last versions

# @login_required
# def page_task_view(request, slug):
#     task = TaskManager.get_or_404(slug__iexact=slug.lower())
#     return render(request, 'details/templates/models_actions/task/task_details.html', context={'task': task})

# @login_required
# @with_developer_access
# def page_subtask_view(request, slug):
#     subtask = SubtaskManager.get_or_404(slug__iexact=slug.lower())
#     return render(request, 'details/subtask_details.html', context={'subtask': subtask})

# @login_required
# @with_manager_access
# def create_task_view(request):
#     form = TaskCreateForm(request.POST or None)
#     if request.method == 'POST' and form.is_valid():
#         form.save()
#         return render(request, 'models_actions/create_model.html',
#                       context={'form': TaskCreateForm(), 'type': 'Task', 'success': True})
#     form.fields["developer"].queryset = UserManager.filter(is_manager=False)
#     return render(request, 'models_actions/create_model.html', context={'form': form, 'type': 'Task'})

# @login_required
# @with_developer_access
# def create_subtask_view(request, slug):
#     form = SubTaskCreateForm(request.POST or None)
#     if request.method == 'POST' and form.is_valid():
#         form.save()
#         return render(request, 'models_actions/create_model.html',
#                       context={'form': SubTaskCreateForm(), 'type': 'Subtask', 'success': True})
#     form.fields["host_task"].queryset = TaskManager.filter(slug__iexact=slug.lower())
#     task_dev_id = TaskManager.get(slug__iexact=slug.lower()).developer.id
#     print(task_dev_id)
#     form.fields["developer"].queryset = UserManager.filter(id=task_dev_id)
#     return render(request, 'models_actions/create_model.html', context={'form': form, 'type': 'Subtask'})

# @login_required
# @with_developer_access
# def delete_subtask_view(request, slug):
#     return delete_or_err404(SubtaskManager, slug)

# def page_project_view(request, slug):
#     project = ProjectManager.get_or_404(slug__iexact=slug.lower())
#     return render(request, 'models_actions/project/project_details.html', context={'project': project})

# @login_required
# @with_developer_access
# def edit_subtask_view(request, slug):
#     return edit_model(request, SubtaskManager, SubTaskCreateForm, slug)

# @login_required
# @with_manager_access
# def delete_task_view(request, slug):
#     return delete_or_err404(TaskManager, slug)

# @login_required
# @with_manager_access
# def edit_task_view(request, slug):
#     return edit_model(request, TaskManager, TaskCreateForm, slug)

# def edit_model(request, model_manager, model_form, slug):
#     if request.method == 'GET':
#         project = model_manager.get_or_404(slug__iexact=slug.lower())
#         return render(request, 'models_actions/create_model.html',
#                       context={'form': model_form(instance=project), 'type': 'Project'})
#     elif request.method == 'POST':
#         form = model_form(request.POST, instance=model_manager.get_or_404(slug__iexact=slug.lower()))
#         if form.is_valid():
#             form.save()
#             return redirect('profile_url')
#         return render(request, 'models_actions/edit_model.html', context={'form': form, 'type': 'Project'})
