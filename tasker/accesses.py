from django.contrib.auth.mixins import AccessMixin


class ManagerRequiredMixin(AccessMixin):
    """Verify that the current user is manger."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_manager:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class DeveloperRequiredMixin(AccessMixin):
    """Verify that the current user is developer."""

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_manager:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

# from tasker.managers import TaskManager, ProjectManager
# class AccessControlToTaskMixin(AccessMixin):
#     """Verify that the user can do smth."""
#
#     def dispatch(self, request, slug):
#         task = TaskManager.safe_get(slug__iexact=slug.lower())
#         if task is request.user.get_all_tasks():
#             return self.handle_no_permission()
#         return super().dispatch(request, slug)
#
#
# class AccessControlToProjectMixin(AccessMixin):
#     """Verify that the user can do smth."""
#
#     def dispatch(self, request, slug):
#         project = ProjectManager.safe_get(slug__iexact=slug.lower())
#         if project is request.user.get_all_projects():
#             return self.handle_no_permission()
#         return super().dispatch(request, slug)
