from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


def user_slug_generate(title):
    return title.lower().replace(' ', '-')


def project_slug_generate(title):
    return title.lower().replace(' ', '-')


def task_slug_generate(title):
    return title.lower().replace(' ', '-')


def subtask_slug_generate(title):
    return title.lower().replace(' ', '-')


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (six.text_type(user.username) +
                six.text_type(timestamp))


account_activation_token = TokenGenerator()
