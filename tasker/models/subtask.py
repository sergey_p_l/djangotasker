from django.db import models
from django.urls import reverse
from tasker.constants import SUBTASK_STATUSES
from tasker.encryptors import subtask_slug_generate
from tasker.managers import SubtaskManager


class Subtask(models.Model):
    """Model class for working with subtasks.

    Fields:
        title - Caption of subtask;
        slug - Short link to refer to a specific object;
        description - Description of subtask;
        host_task - Reference to the host task;
        status - Status of the current subtask. Settings in "/constants.py", SUBTASK_STATUSES;
        developer - Reference to the executing developer;

    """
    title = models.CharField(max_length=32, unique=True)
    slug = models.CharField(max_length=32)
    description = models.TextField()
    host_task = models.ForeignKey(to='Task', on_delete=models.CASCADE, related_name='subtasks')
    status = models.PositiveSmallIntegerField(default=0)
    developer = models.ForeignKey(to='User', on_delete=models.CASCADE, related_name='subtasks')

    objects = SubtaskManager

    def __str__(self):
        return self.title

    def mark_completed(self):
        if self.status == SUBTASK_STATUSES['Complete']:
            self.status = SUBTASK_STATUSES['Default']
        else:
            self.status = SUBTASK_STATUSES['Complete']
        self.save()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Subtask, self).save(force_insert, force_update, using, update_fields)
        self.slug = subtask_slug_generate(self.title)
        super(Subtask, self).save()

    def mark_canceled(self):
        if self.status == SUBTASK_STATUSES['Canceled']:
            self.status = SUBTASK_STATUSES['Default']
        else:
            self.status = SUBTASK_STATUSES['Canceled']
        self.save()

    def task_status(self):
        for i, j in SUBTASK_STATUSES.items():
            if self.status == j:
                return i
        return 'Default'

    def get_absolute_url(self):
        return reverse('subtask_details_url', kwargs={'slug': self.slug})

    def get_delete_link(self):
        return reverse('delete_subtask', kwargs={'slug': self.slug})
