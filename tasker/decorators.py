from django.shortcuts import redirect


def with_manager_access(func_extend):
    def check_manager_access(request, *args, **kwargs):
        if request.user.is_manager:
            return func_extend(request, *args, **kwargs)
        else:
            return redirect('access_error_url')

    return check_manager_access


def with_developer_access(func_extend):
    def check_developer_access(request, *args, **kwargs):
        if not request.user.is_manager:
            return func_extend(request, *args, **kwargs)
        else:
            return redirect('access_error_url')

    return check_developer_access

# from tasker.managers import TaskManager, ProjectManager
#
# def with_reach_check(func_extend):
#     def reach_check(request, obj_slug, *args, **kwargs):
#         if TaskManager.safe_get(slug__iexact=obj_slug.lower()) in request.user.get_all_tasks() or \
#                 ProjectManager.safe_get(slug__iexact=obj_slug.lower()) in request.user.get_all_projects():
#             return func_extend(request, *args, **kwargs)
#         else:
#             return redirect('access_error_url')
#
#     return reach_check
